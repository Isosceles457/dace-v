create database dace;

USE dace;

CREATE TABLE `teachers` (
  `id` int AUTO_INCREMENT primary key NOT NULL,
  `name` varchar(40) NOT NULL,
  `lastname` varchar(40) NOT NULL,
  `age` int(2) NOT NULL,
  `ci` int(8) NOT NULL unique,
  `mail` varchar(320) NOT NULL unique,
  `phone` varchar(10) NOT NULL unique,
  `sex` varchar(1) NOT NULL,
  `password` varchar(100) NOT NULL unique
);

INSERT INTO `teachers` (`id`, `name`, `lastname`, `age`, `ci`, `mail`, `phone`, `sex`, `password`) 
VALUES(1, 'Cherry Junior', 'Ramirez Oliveros', 39, 20979444, 'examplemail@gmail.com', '4123918441', "M", "pr0fes0r");

CREATE TABLE `users` (
  `id` int primary key AUTO_INCREMENT NOT NULL,
  `name` varchar(40) NOT NULL,
  `lastname` varchar(40) NOT NULL,
  `age` int(2) NOT NULL,
  `ci` int(8) NOT NULL unique,
  `mail` varchar(320) NOT NULL unique,
  `phone` varchar(10) NOT NULL unique,
  `sex` varchar(1) NOT NULL,
  `uca` int(2),
  `password` varchar(100) NOT NULL unique
);

CREATE TABLE `help` (
  `id` int primary key AUTO_INCREMENT NOT NULL,
  `name` varchar(40) NOT NULL,
  `message` varchar(200) NOT NULL,
  `date` date not null
);

CREATE TABLE `grades`(
  `id` int primary key AUTO_INCREMENT NOT NULL,
  `id_user` int not null,
  `assessment` int not null,
  `id_pensum` int not null
)

CREATE TABLE `pensum`(
  `id` int primary key AUTO_INCREMENT NOT NULL,
  `name` varchar(40) NOT NULL,
  `uc` int,
  `code` varchar(40) NOT NULL,
  `career_id` int not null
)

CREATE TABLE `career`(
  `id` int primary key AUTO_INCREMENT NOT NULL,
  `name` varchar(40) NOT NULL,
  `code` varchar(40) NOT NULL
)

INSERT INTO `pensum`(name, uc, code, career_id) values('Programación I', 4, "IC3244", 1),("FUNDAMENTOS DE LA INFORMATICA", 3, "IC1222", 1),
("DEPORTE", 2, "DP0001",1), ("MATEMATICA I", 5, "IM1421", 1), ("LOGICA MATEMATICA", 5, "IM1223", 1), ("FORMACION CONSTITUCIONAL", 2, "FC0001", 1),
("LENGUAJE Y COMUNICACION", 2, "IH1124", 1), ("INGLES I", 2, "IH1125", 1), ("ECONOMIA DIGITAL", 2, "EC005", 1),
("MATEMATICA II", 5, "IM2421", 1)

INSERT INTO `grades`(id_user, id_pensum, assessment) values(1, 1, 8)

INSERT INTO `career`(name, code) values("Ingenieria en Informática", 601)

CREATE TABLE `period`(
  `id` int primary key AUTO_INCREMENT NOT NULL,
  `name` varchar(40) NOT NULL,
  `status` varchar(40) NOT NULL
)

CREATE TABLE `inscription`(
  `id` int primary key AUTO_INCREMENT NOT NULL,
  `id_user` int,
  `id_period` int
)

CREATE TABLE `subject`(
  `id` int primary key AUTO_INCREMENT NOT NULL,
  `id_pensum` int,
  `id_inscription` int
)

select users.name, users.ci, career.name, career.code, pensum.name, pensum.uc, grades.assessment from users

join grades on(id_user = users.id)
join pensum on(grades.id_pensum = pensum.id)
join career on(pensum.career_id = career.id);

update notes
set = ?
where (
  join pensum on(id_pensum = pensum.id))
