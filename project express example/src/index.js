const express = require('express');
const path = require('path');
const morgan = require('morgan');

class App {
  constructor() {
    this.express = express();
    this.setupMiddleware();
    this.setupRoutes();
    this.setupViews();
    this.setupStaticFiles();
  }

  setupMiddleware() {
    this.express.use(morgan('dev'));
    this.express.use(express.urlencoded({ extended: false }));
  }

  setupRoutes() {
    this.express.use((req, res, next) => {
      next();
    });
    this.express.use(require('./routes/index'));
    this.express.use('/auth', require('./routes/auth'));
    this.express.use('/link', require('./routes/link'));
  }

  setupViews() {
    this.express.set('views', path.join(__dirname, 'views'));
    this.express.set('view engine', '.ejs');
  }

  setupStaticFiles() {
    this.express.use(express.static(path.join(__dirname, 'public')));
  }

  start() {
    this.express.listen(3000, () => console.log('Server started on port 3000'));
  }
}

const app = new App()
app.start()

module.exports = App;

