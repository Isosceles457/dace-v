const express = require('express');
const router = express.Router();
const axios = require('axios');
const pool = require('../database');
const secret = "6Lf7WAAqAAAAALCv-85gvElRVOvvr3xu55awbJo5";
const bcrypt = require('bcryptjs')
const Event = require('events');

class UserRegistration extends Event {
  constructor() {
    super();
    this.secret = secret;
  }

async verifyReCaptcha(token) {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject(new Error('reCAPTCHA not checked'));
    } else {
      axios.post('https://www.google.com/recaptcha/api/siteverify', null, {
        params: {
          secret: this.secret,
          response: token
        }
      })
      .then(response => {
        console.log('Token:', token); // Log the token value
        console.log('reCAPTCHA API response:', response.data);
        if (response.data.success) {
          resolve(response.data);
        } else {
          reject(new Error('reCAPTCHA verification failed'));
        }
      })
      .catch(error => {
        console.error('Error verifying reCaptcha:', error);
        reject(error);
      });
    }
  });
}


async registerUser(userData) {
  const { name, lastname, age, ci, mail, phone, sex, password } = userData;
  const newUser = { name, lastname, age, ci, mail, phone, sex };

  try {
    const reCaptchaResponse = await this.verifyReCaptcha(userData['g-recaptcha-response']);

    // Hash the password
    const salt = await bcrypt.genSalt(8);
    newUser.password = await bcrypt.hash(password, salt);

    await pool.query("INSERT INTO users SET ?", [newUser]);
    this.emit('userRegistered', newUser); // Emit the 'userRegistered' event
    return newUser;
  } catch (err) {
    if (err.message === 'reCAPTCHA not checked') {
      throw new Error('reCAPTCHA not checked');
    } else if (err.message === 'reCAPTCHA verification failed') {
      throw new Error('reCAPTCHA verification failed');
    } else if (err.code === "ER_DUP_ENTRY") {
      await this.errorReports(err);
    } else {
      console.error('Error creating user:', err);
      throw new Error("An error occurred while creating the user");
    }
  }
}


errorReports(err) {
  return new Promise((resolve, reject) => {
    let errorMessage = "Duplicate entry";
    let duplicatedFields = [];

    if (err.message.includes('mail')) {
      duplicatedFields.push('email');
    }

    if (err.message.includes('phone')) {
      duplicatedFields.push('phone');
    }

    if (err.message.includes('ci')) {
      duplicatedFields.push('identification');
    }
console.log(duplicatedFields.length)
    if (duplicatedFields.length == 1) {
      errorMessage = `Duplicate ${duplicatedFields[0]}`;
    } else if (duplicatedFields.length == 2) {
      errorMessage = `Duplicate ${duplicatedFields[0]} and ${duplicatedFields[1]}`;
    } else if (duplicatedFields.length == 3) {
      errorMessage = `Duplicate ${duplicatedFields[0]}, ${duplicatedFields[1]}, and ${duplicatedFields[2]}`;
    }

    reject(new Error(errorMessage));
  });
}

}

router.get('/inicio', (req, res) => {
  res.render('auth/inicio');
});

router.post("/inicio", async (req, res) => {
  try {
    const userRegistration = new UserRegistration();
    userRegistration.on('userRegistered', (newUser) => {
      console.log('New user registered:', newUser);
    });
    const newUser = await userRegistration.registerUser(req.body);
    res.status(201).json(newUser);
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
});

module.exports = router;

