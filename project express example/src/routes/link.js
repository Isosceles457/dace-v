
const express = require('express')
const router = express.Router()
const pool = require('../database')
const axios = require('axios');
const secret = "6Lf7WAAqAAAAALCv-85gvElRVOvvr3xu55awbJo5"




router.post("/add", async (req, res) => {
  const {
    name,
    lastname,
    age,
    ci,
    mail,
    phone,
    sex,
    password,
    'g-recaptcha-response': token
  } = req.body;

  const newLink = {
    name,
    lastname,
    age,
    ci,
    mail,
    phone,
    sex,
    password,
  };

  try {
    // Verify the reCAPTCHA response
    const response = await axios.post('https://www.google.com/recaptcha/api/siteverify', null, {
      params: {
        secret,
        response: token
      }
    });
  console.log('Token:', token); // Log the token value
    console.log('reCAPTCHA API response:', response.data);

    if (response.data.success) {
      // reCAPTCHA verification successful, proceed with form submission
      await pool.query("INSERT INTO users SET ?", [newLink]);
      res.status(201).json(newLink);
    } else {
      // reCAPTCHA verification failed
      res.status(400).json({ error: 'reCAPTCHA verification failed' });
    }
  } catch (err) {
    if (err.code === "ER_DUP_ENTRY") {
      res.status(409).json({ error: "Duplicate" });
    } else {
      res.status(500).json({ error: "An error occurred while creating the user" });
    }
  }
});


router.post("/add",async (req, res) => {
    const {
        name,
        lastname,
        age,
        ci,
        mail,
        phone,
        sex,
        password,
    } = req.body
    const newLink ={
            name,
            lastname,
            age,
            ci,
            mail,
            phone,
            sex,
            password,
        
    }
    await pool.query('INSERT INTO users set ?', [newLink])
    res.send(newLink)
})

router.get('/', async (req, res) => {
    const users = await pool.query('SELECT * FROM users')
    console.log(users);
    res.send(users)
})
module.exports = router

